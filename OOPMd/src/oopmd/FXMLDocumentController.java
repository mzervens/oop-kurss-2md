
package oopmd;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.util.Pair;


/**
 *  U219. Sastādīt Java sīklietotni, kas uzzīmē kvadrātu un ļauj lietotājam ievadīt pārbīdes lielumus x un y. 
 *        Pēc lielumu x un y ievadīšanas jāuzklāj eksistējošam zīmējumam virsū vēl viens kvadrāts, 
 *        kas pabīdīts par x vienībām x-ass virzienā un par y vienībām y-ass virzienā.
 * 
 * @author Matiss Zervens
 */
public class FXMLDocumentController implements Initializable 
{ 
    @FXML
    private Canvas canvas;
    @FXML
    private TextField txtFldXOffset;
    @FXML
    private TextField txtFldYOffset;
    @FXML
    private Button btnDraw;
    
    @FXML
    private AnchorPane canvasParent;
    
    private FXMLDocumentDrawingHelper drawingHelpers = new FXMLDocumentDrawingHelper();
    
    private ArrayList<Pair<Double, Double>> drawnRects = new ArrayList<>();
    private final double dimensions = 50.0;
    private Pair<Double, Double> oldParentDims = new Pair<>(0.0, 0.0);
    private Pair<Double, Double> origin;
    
    @FXML
    private void drawBtnAction(ActionEvent event) 
    {
        Pair<Double, Double> newParentDims = new Pair<>(canvasParent.getWidth(), canvasParent.getHeight());
        resizeCanvasIfNeeded(newParentDims);
        
        GraphicsContext ctx = canvas.getGraphicsContext2D();
        
        Pair<Double, Double> offsets = getInputValues();
        
        double topLeftX = origin.getKey() - (dimensions / 2) + offsets.getKey();
        double topLeftY = origin.getValue() - (dimensions / 2) + offsets.getValue();
        
        Pair<Double, Double> topLeftCoors = limitToCanvasDim(topLeftX, topLeftY);
        drawnRects.add(topLeftCoors);
        
        ctx.setFill(Color.DARKSEAGREEN);
        ctx.fillRect(topLeftCoors.getKey(), topLeftCoors.getValue(), dimensions, dimensions);
        
        oldParentDims = newParentDims;
    }
    
    /**
     * Called when the mouse leaves or enters the scene, 
     * calls canvas resizing functionality
     */
    @FXML
    private void resizeAction()
    {
        Pair<Double, Double> newParentDims = new Pair<>(canvasParent.getWidth(), canvasParent.getHeight());
        resizeCanvasIfNeeded(newParentDims);
    }
    
    /**
     * Resizes the canvas if its parents size has changed
     * @param newParentDims 
     */
    private void resizeCanvasIfNeeded(Pair<Double, Double> newParentDims)
    {   
        if(!oldParentDims.equals(newParentDims))
        {
            setNewCanvasDims(newParentDims.getKey(), newParentDims.getValue());
        }
    }
    
    /**
     * Initializes the origin point from which all shapes are drawn from
     */
    private void initOrigin()
    {
        double centerX = canvas.getWidth() / 2;
        double centerY = canvas.getHeight() / 2;
        
        origin = new Pair<>(centerX, centerY);
    }
    
    /**
     * Resizes the canvas and redraws its contents
     * @param width
     * @param height 
     */
    private void setNewCanvasDims(double width, double height)
    {
        canvas.setWidth(width - 4);
        canvas.setHeight(height - 4);
        redraw();
        drawingHelpers.drawBorderAroundCanvas(canvas.getWidth(), canvas.getHeight(), canvas.getGraphicsContext2D());
    }
    
    /**
     * Clears the canvas and redraws all previously drawn rectangles
     */
    private void redraw()
    {
        GraphicsContext ctx = canvas.getGraphicsContext2D();
        ctx.clearRect(0, 0, canvas.getWidth(), canvas.getHeight());
    
        ctx.setFill(Color.DARKSEAGREEN);
        for(int i = 0; i < drawnRects.size(); i++)
        {
            Pair<Double, Double> topLeftCoord = limitToCanvasDim(drawnRects.get(i).getKey(), drawnRects.get(i).getValue());
            ctx.fillRect(topLeftCoord.getKey(), topLeftCoord.getValue(), dimensions, dimensions);
            
            drawnRects.set(i, topLeftCoord);
        }
    }
    
    /**
     * Clamps the squares coordinates so that the square entirely fits 
     * into the canvas area
     * 
     * @param topLeftX
     * @param topLeftY
     * @return 
     */
    private Pair<Double, Double> limitToCanvasDim(double topLeftX, double topLeftY)
    {
        double height = canvas.getHeight();
        double width = canvas.getWidth();
        
        if(topLeftX + dimensions > width)
        {
            double diff = (topLeftX + dimensions) - width;
            topLeftX -= diff;
        }
        if(topLeftX < 0)
        {
            topLeftX = 0;
        }
        
        if(topLeftY < 0)
        {
            topLeftY = 0;
        }
        if(topLeftY + dimensions > height)
        {
            double diff = (topLeftY + dimensions) - height;
            topLeftY -= diff;
        }
        
        return new Pair<>(topLeftX, topLeftY);
        
    }
    
    /**
     * Fetches the offset values from the UI
     * @return 
     */
    private Pair<Double, Double> getInputValues()
    {
        double xOffset;
        double yOffset;
        try
        {
            xOffset = Double.parseDouble(txtFldXOffset.getText().trim());
            
        }
        catch(Exception e)
        {
            xOffset = 0;
            txtFldXOffset.setText(Double.toString(xOffset));
        }
        
        try
        {
            yOffset = Double.parseDouble(txtFldYOffset.getText().trim());
        }
        catch(Exception e)
        {
            
            yOffset = 0;
            txtFldYOffset.setText(Double.toString(yOffset));
        }
        
        Pair<Double, Double> p = new Pair<>(xOffset, yOffset);
        return p;
    }
    
    
    @Override
    public void initialize(URL url, ResourceBundle rb) 
    {
        initOrigin();
        drawingHelpers.drawBorderAroundCanvas(canvas.getWidth(), canvas.getHeight(), canvas.getGraphicsContext2D());
        drawBtnAction(null);
    }    
    
}
