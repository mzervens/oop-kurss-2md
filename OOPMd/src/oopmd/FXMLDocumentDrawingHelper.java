

package oopmd;

import javafx.scene.canvas.GraphicsContext;


public class FXMLDocumentDrawingHelper 
{
    /**
     * Draws a line border around the canvas
     */
    public void drawBorderAroundCanvas(double width, double height, GraphicsContext ctx)
    {      
        ctx.strokeLine(0, 0, width, 0);
        ctx.strokeLine(0, 0, 0, height);
        
        ctx.strokeLine(width, height, width, 0);
        ctx.strokeLine(width, height, 0, height);
    }
}
